$(function(){
	$(".menu,.main-nav-close").on("click",function(){
		$(".main-nav").slideToggle();
	})
	$(".quick-links-close").on("click",function(){
		$(".quick-links").hide();
		$(".quick-links-show").show();
	})
	$(".quick-links-close-m").on("click",function(){
		$(".top-tip").hide();
		$(".quick-links-show").show();
	})
	$(".quick-links-show").on("click",function(){
		if(window.innerWidth>1024){
			$(".quick-links").show();
		}else{
			$(".top-tip").show();
		}
		$(".quick-links-show").hide();
	})
	$(".back").on("click",function(){
		history.back();
	})
	$(".flex-wrap-close").on("click",function(){
		$(".flex-wrap").removeClass("action");
	})
	$(".flex-wrap-show").on("click",function(){
		$(".flex-wrap-info").addClass("action");
	})
	$(".flex-wrap-rasie-show").on("click",function(){
		$(".flex-wrap-rasie").addClass("action");
	})
	if($("#index").length>0){
		$.ajax({
			type : 'GET',
			url : "json/info.json?v="+Math.floor(Math.random()*10000),
			//url : "https://taiwanpay-2020event.firebaseio.com/data.json",
			dataType : "json",
			error : function (xhr, textStatus, errorthrown) {
				
			},
			success : function (data, status, xhr) {
				/*if(data.url=="" || data.url=="none" || data.url==null){
					$(".info").html(data.msg);
				}else{
					var target = (data.target==1)?' target="_blank"':'';
					$(".info").html('<a'+target+' href="'+data.url+'">'+data.msg+'</a>');
				}*/
				var info = "";
				var infoLength=0;
				for(var i=1;i<data.length;i++){
					if(data[i].url==null){
						info+='<div>'+data[i].msg+'</div>'
					}else{
						var target = (data[i].target!=null)?' target="_blank"':'';
						info+='<div><a'+target+' href="'+data[i].url+'">'+data[i].msg+'</a></div>';
					}
					infoLength+=1;
				}
				$(".info").html(info);
				infoRun(1,infoLength);
			}
		});
	}
	var infoRun = function(num,size){
		$(".info div:nth-of-type("+num+")").addClass("action").siblings().removeClass("action");
		if($(".info.action div.action a").width() + 30 > $(".info.action div.action").width()){
			$(".info.action div.action a").css("margin-left","100%").animate({marginLeft:$(".info.action div.action").width() - $(".info.action div.action a").width() + "px"},7000);
			(num + 1 > size)?num=1:num+=1;
			setTimeout(function(){infoRun(num,size)},8000);
		}else{
			(num + 1 > size)?num=1:num+=1;
			setTimeout(function(){infoRun(num,size)},5000);
		}
		
	}
	$(window).scroll(function() {
		if($(window).scrollTop()>window.innerHeight){
			$(".gototop").show();
		}else{
			$(".gototop").hide();
		}
	});
	$(".gototop").on("click",function(){
		$(window).scrollTop(0);
	})
	/*$(".info").on("mouseover",function(){
		var highestTimeoutId = setTimeout(";");
		for (var i = 0 ; i < highestTimeoutId ; i++) {
		    clearTimeout(i); 
		}
	})
	$(".info").on("mouseout",function(){
		$(".info div.action").index()
		infoRun($(".info div.action").index(),$(".info div").length / $(".info").length);
	})*/
	$(".info").removeClass("action");
	if(window.innerWidth>1024){
		$(".info.mobile-hide").addClass("action");
	}else{
		$(".info.pc-hide").addClass("action");
	}
})